var events = require('events');
var util = require('util');

function isStartOfSession(str) {
  return /FXO:\sStart\sCNDD/.test(str);
}

function isEndOfSession(str) {
  return /Stop\sPSTN\sTone/.test(str);
}

function isPhoneNumber(str) {
  return /FXO: CNDD Name= Phone=[0-9]+/.test(str);
}

function extractIP(str) {
  var r = /^[^\s]+\s[^\s]+\s[^\s]+\s([^\s]+)/;
  var matches = r.exec(str);
  if (matches !== null && matches.length > 1) {
    return matches[1];
  }
  return null;
}

function extractPhoneNumber(str) {
  var matches = /FXO: CNDD Name= Phone=([0-9]+)/.exec(str);
  if (matches !== null && matches.length > 1) {
    return matches[1];
  }
  return null;
}

var sessions = {};

function LogParser() {
  events.EventEmitter.call(this);
}
util.inherits(LogParser, events.EventEmitter);

LogParser.prototype.parse = function(str) {
  var ip = extractIP(str);
  var session;

  if (isStartOfSession(str)) {
    sessions[ip] = {
      ip: ip,
      phone: ''
    };

    this.emit('sessionStart', sessions[ip]);
    return;
  }

  if (isEndOfSession(str)) {
    session = sessions[ip];
    if (session === null || typeof session === 'undefined') {
      return;
    }
    sessions[ip] = null;

    this.emit('sessionEnd', session);
    return;
  }

  if (isPhoneNumber(str)) {
    session = sessions[ip];
    if (session === null || typeof session === 'undefined') {
      return;
    }

    session.phone = extractPhoneNumber(str);
    this.emit('sessionPhone', session);
  }
};

module.exports = LogParser;

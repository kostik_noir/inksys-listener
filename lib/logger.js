function log(session, prefix) {
  console.log(prefix + '\tip:' + session.ip + '\tphone:' + session.phone);
}

module.exports = {
  onPhone: function(session) {
    log(session, '[on phone]');
  },
  onEnd: function(session) {
    log(session, '[on end]');
  }
};

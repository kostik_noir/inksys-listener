var Listener = require('./Listener');
var LogParser = require('./LogParser');
var logger = require('./logger');

var api = {};

api.create = function addListener(host, port) {
  var listener = new Listener(host, port);
  var parser = new LogParser();

  parser.on('sessionEnd', function(session) {
    logger.onEnd(session);
  });

  parser.on('sessionPhone', function(session) {
    logger.onPhone(session);
  });

  listener.on('message', function(msg) {
    parser.parse(msg);
  });
};

module.exports = api;

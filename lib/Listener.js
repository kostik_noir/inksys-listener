var events = require('events');
var util = require('util');
var dgram = require('dgram');

function createServer(listener) {
  var server = dgram.createSocket('udp4');
  server.on('message', listener._onMessage.bind(listener));
  server.bind(listener.port, listener.host);
  listener.server = server;
}

function Listener(host, port) {
  events.EventEmitter.call(this);

  this.host = host;
  this.port = port;
  createServer(this);
}
util.inherits(Listener, events.EventEmitter);

Listener.prototype.host = null;

Listener.prototype.port = null;

Listener.prototype.server = null;

Listener.prototype._onMessage = function(message) {
  this.emit('message', message.toString());
};

module.exports = Listener;

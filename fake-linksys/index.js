var dgram = require('dgram');
var fs = require('fs');
var config = require('../config');

var HOST = config[0].host;
var PORT = config[0].port;

var logTemplate = fs.readFileSync(__dirname + '/log-tmpl.txt', {
  encoding: 'utf8'
});

var sender = dgram.createSocket('udp4');

function delayNextChunk(cb) {
  cb();
}

function addFakePhoneNumber(log) {
  var countOfDigits = 10;
  var digits = [];
  for (var i = 0; i < countOfDigits; i++) {
    digits.push(Math.floor(Math.random() * 10));
  }

  return log.replace(/(FXO: CNDD Name= Phone=)[0-9]+/, '$1' + digits.join(''));
}

function addFakeIp(log) {
  var countOfParts = 4;
  var ipParts = [];
  var digits;
  var countOfDigits;
  var j;

  for (var i = 0; i < countOfParts; i++) {
    countOfDigits = Math.floor(Math.random() * 2) + 1;
    digits = [];

    for (j = 0; j < countOfDigits; j++) {
      digits.push(Math.floor(Math.random() * 10));
    }

    ipParts.push(digits.join(''));
  }

  return log.replace(/[^\s\.]+\.[^\s\.]+\.[^\s\.]+\.[^\s\.]+/g, ipParts.join('.'));
}

function sendLog(cb) {
  var log = addFakePhoneNumber(logTemplate);
  log = addFakeIp(log);

  var lines = log.split('\n');
  var lineIndex = 0;

  var maxDelay = 50;
  var delay;

  function sendLine() {
    if (lineIndex >= lines.length) {
      console.log('log sent');
      cb();
      return;
    }

    var message = new Buffer(lines[lineIndex]);
    sender.send(message, 0, message.length, PORT, HOST);
    lineIndex++;

    delay = Math.floor(Math.random() * maxDelay);
    setTimeout(sendLine, delay);
  }

  sendLine();
}

function sendNextLog() {
  sendLog(sendNextLog);
}

sendNextLog();

var lib = require('./lib');
var config = require('./config');

config.forEach(function(obj) {
  lib.create(obj.host, obj.port);
});

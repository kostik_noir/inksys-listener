# Proof of concept for https://www.elance.com/j/javascript-guru-experienced-angularjs-nodejs/78436443/

## Start fake Linksys server

```
node ./fake-linksys/
```

## Start listener

```
node index.js
```

It will use 

+ host: 'localhost'
+ port: 3000

The fake server:

+ it will send the content of the _./fake-linksys/log-tmpl.txt_ line by line
+ it will use different values for IP and phone number for each "call"

The client will log "on phone number received" and "on call ended" into console. Sure it is possible to send 
data to another server.
